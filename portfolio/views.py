from datetime import datetime

from django.views.generic import TemplateView
from tiles.models import Project
from django.db.models import Q


class HomePageView(TemplateView):
    template_name = "home.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["actual_date"] = datetime.now()
        context["projects"] = Project.objects.all()
        qeuery = Q(id__gt=0) | Q(id__lte=10)
        context["projects_filtered1"] = Project.objects.filter(qeuery)
        context["projects_filtered2"] = Project.objects.filter(category__name="ABC")
        context["projects_filtered3"] = Project.objects.get_project_this_years
        print(context)
        return context


class AboutPageView(TemplateView):
    template_name = "about.html"
