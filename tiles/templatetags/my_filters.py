from django import template
from django.template.defaultfilters import stringfilter
import markdownx.utils

register = template.Library()


@register.filter
@stringfilter
def markdownify(value):
    return markdownx.utils.markdownify(value)
