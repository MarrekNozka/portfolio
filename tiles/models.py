from django.db import models

# from django.forms import CharField, URLField
from tiles.managers import ProjectManager
from colorfield.fields import ColorField
from markdownx.models import MarkdownxField


# Create your models here.
class BaseModel(models.Model):
    class Meta:
        abstract = True

    created_at = models.DateTimeField(auto_now_add=True)
    modify_at = models.DateTimeField(auto_now=True)


class Tag(BaseModel):
    class Meta:  # pyright: ignore
        verbose_name = "Tag"
        verbose_name_plural = "Tagy"

    name = models.CharField(primary_key=True, max_length=200, verbose_name="Název")
    description = models.TextField(blank=True, null=True, verbose_name="Popisek")
    icon = models.ImageField(upload_to="tag_icons", verbose_name="Tag Icon", blank=True)
    color = ColorField(default="#000000", blank=True, image_field="icon")

    def __str__(self):
        return f"{self.name}"


class Category(BaseModel):
    class Meta:  # pyright: ignore
        verbose_name = "Kategorie"
        verbose_name_plural = "Kategorie"

    name = models.CharField(primary_key=True, max_length=200, verbose_name="Název")
    description = models.TextField(blank=True, null=True)
    icon = models.ImageField(
        upload_to="category_icons",
        verbose_name="Category Icon",
        blank=True,
    )

    def __str__(self):
        return f"{self.name}"


class Project(BaseModel):
    objects = ProjectManager()

    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=100, verbose_name="Jméno")
    description = models.TextField(
        verbose_name="Popis",
        blank=True,
        null=True,
    )
    category = models.ForeignKey(
        Category,
        on_delete=models.RESTRICT,
        related_name="kategorie",
        null=True,
    )
    tags = models.ManyToManyField(Tag, related_name="tagy", blank=True)

    content = MarkdownxField(verbose_name="Obsah", default="#Nadpis\n")

    url = models.URLField(
        max_length=200, verbose_name="Project URL", blank=True, null=True
    )
    logo = models.ImageField(upload_to="logo_images", verbose_name="Project logo")

    class Meta:  # pyright: ignore
        verbose_name = "Projekt"
        verbose_name_plural = "Projekt"

    def __str__(self):
        return f"{self.name}"
