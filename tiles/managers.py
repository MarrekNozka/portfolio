from django.db import models
from django.utils.timezone import now


class ProjectManager(models.Manager):
    def get_project_this_years(self, year=None):
        year = year or now()
        return self.get_queryset().filter(created_at__year=year)
