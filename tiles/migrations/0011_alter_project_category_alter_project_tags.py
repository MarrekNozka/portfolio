# Generated by Django 5.0.6 on 2024-05-23 07:22

import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("tiles", "0010_remove_project_type_project_category_project_tags"),
    ]

    operations = [
        migrations.AlterField(
            model_name="project",
            name="category",
            field=models.ForeignKey(
                null=True,
                on_delete=django.db.models.deletion.RESTRICT,
                related_name="kategorie",
                to="tiles.category",
            ),
        ),
        migrations.AlterField(
            model_name="project",
            name="tags",
            field=models.ManyToManyField(
                null=True, related_name="tagy", to="tiles.tag"
            ),
        ),
    ]
