from django.contrib import admin

# Register your models here.
from tiles.models import Tag, Category, Project
from markdownx.admin import MarkdownxModelAdmin


@admin.register(Tag)
class TagaAdmin(admin.ModelAdmin):
    list_display = ("name", "description")
    search_fields = ("name", "description")
    readonly_fields = ("created_at", "modify_at")


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    list_display = ("name", "description")
    search_fields = ("name", "description")
    readonly_fields = ("created_at", "modify_at")


# admin.site.register(Project)
@admin.register(Project)
class ProjectAdmin(MarkdownxModelAdmin):
    list_display = ("name", "description", "url", "logo")
    search_fields = ("name", "description", "url")
    list_filter = ("category",)
    list_per_page = 33
    readonly_fields = ("created_at", "modify_at")
